const CryptoAPI = require('../lib/CryptoAPI');
const KeyManager = require('../lib/KeyManager');

const check = {
  async price(cmd) {
    try {
      // Formatter for currency
      const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: cmd.currency,
      });

      const keyManager = new KeyManager();
      const key = keyManager.getKey();

      const api = new CryptoAPI(key);
      const priceData = await api.getPriceData(cmd.coin, cmd.currency);

      let output = '';

      priceData.forEach(coin => {
        output += `Coin: ${coin.symbol.yellow} (${coin.name}) | Price: ${
          formatter.format(coin.price).green
        } | Rank: ${coin.rank.blue}\n`;
      });

      console.log(output);
    } catch (error) {
      console.error(error.message.red);
    }
  },
};

module.exports = check;
