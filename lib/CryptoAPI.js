const axios = require('axios');
const colors = require('colors');

class CryptoAPI {
  constructor(apiKey) {
    this.apiKey = apiKey;
    this.baseUrl = 'https://api.nomics.com/v1/currencies/ticker';
  }

  async getPriceData(coin, currency) {
    try {
      const res = await axios.get(
        `${this.baseUrl}?key=${this.apiKey}&ids=${coin}&convert=${currency}`
      );

      return res.data;
    } catch (error) {
      handleAPIError(error);
    }
  }
}

function handleAPIError(err) {
  if (err.response.status === 401) {
    throw new Error('Your API key is invalid');
  } else if (err.response.status === 404) {
    throw new Error('The API is not responding');
  } else {
    throw new Error('Something is not working');
  }
}

module.exports = CryptoAPI;
