#!/usr/bin/env node
const program = require('commander');
const packagejson = require('../package.json');

program
  .version(packagejson.version)
  .command('key', 'manage API key -- https://nomics.com')
  .command('check', 'check coin price info')
  .parse(process.argv);
